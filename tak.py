import re
import numpy as np

WHITE = 1
BLACK = -1

FLAT = 1
STANDING = 2
CAP = 3
stone_types = [FLAT, STANDING, CAP]
stone_type_chars = ['F', 'S', 'C']

NORTH, EAST, SOUTH, WEST = 0, 1, 2, 3
direction_ids = [NORTH, EAST, SOUTH, WEST]
direction_chars = ['+', '>', '-', '<']
direction_vectors = [(0, 1), (1, 0), (0, -1), (-1, 0)]

distributions = [[1, 0, 0, 0],
                [1, 1, 0, 0], [2, 0, 0, 0],
                [1, 1, 1, 0], [1, 2, 0, 0], [2, 1, 0, 0], [3, 0, 0, 0],
                [1, 1, 1, 1], [1, 1, 2, 0], [1, 2, 1, 0], [2, 1, 1, 0], [1, 3, 0, 0], [3, 1, 0, 0], [4, 0, 0, 0],
                [1, 1, 1, 2], [1, 1, 2, 1], [1, 2, 1, 1], [2, 1, 1, 1], [1, 1, 3, 0], [1, 3, 1, 0], [3, 1, 1, 0], [1, 4, 0, 0], [4, 1, 0, 0], [5, 0, 0, 0]]
distributions = [np.array(distr) for distr in distributions]

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']


def parse_action_string(action_str):
    lookup_char_coord = {c: i for i, c in enumerate(alphabet)}
    lookup_char_direction_id = dict(zip(direction_chars, direction_ids))
    lookup_str_distribution_id = {''.join([str(de) for de in d]): i for i, d in enumerate(distributions)}
    lookup_char_stone_type = dict(zip(stone_type_chars, stone_types))

    pattern = re.compile(r'(?:([a-e])([1-5])([\+\>\-\<])([0-5]{4}))|(?:(F|S|C)([a-e])([1-5]))')
    match = pattern.match(action_str)
    placement, movement = None, None
    if match:
        if None not in match.group(1, 2, 3, 4):
            x = lookup_char_coord[match.group(1)]
            y = int(match.group(2)) - 1
            dir_id = lookup_char_direction_id[match.group(3)]
            distr_id = lookup_str_distribution_id[match.group(4)]
            movement = x, y, dir_id, distr_id
        elif None not in match.group(5, 6, 7):
            stone_type = lookup_char_stone_type[match.group(5)]
            x = lookup_char_coord[match.group(6)]
            y = int(match.group(7)) - 1
            placement = x, y, stone_type
    else:
        raise ValueError("Could not parse action string. Make sure you enter valid PTN.")

    action = placement, movement
    return action


def action_string(action):
    """
    The action_string function converts an action - a tuple of the form (placement, movement) - into a string.
    The placement is an optional element of the tuple and if present, contains three elements:
    the x-coordinate, y-coordinate and stone_type. The stone_type can be one of three types: 
    STANDING STONE ('S'), FLAT STONE ('F') or CAP stone ('C'). If no placement is given then only 
    a movement will be present in the action string. A movement consists of four elements: x-coordinate, 
    y-coordinate, direction id and distribution id. The direction id can take on values 0 through 3 corresponding to
    north ('+'), east ('>'), south ('-') or west ('<').
    Distribution id takes on values from 0 through len(distributions) - 1 and is converted to a string of integers that
    correspond to the number of stones that are left on all successive tiles that are traversed when moving into the given direction.
    
    :param action: The action to be converted.
    :return: A string that represents the action.
    """
    s = ""
    placement, movement = action
    if placement is not None and movement is None:
        x, y, stone_type = placement
        stone_type_str = 'F'
        if stone_type == STANDING:
            stone_type_str = 'S'
        elif stone_type == CAP:
            stone_type_str = 'C'
        s = stone_type_str + alphabet[x] + str(y + 1)
    elif placement is None and movement is not None:
        x, y, direction_id, distribution_id = movement
        dir_str = direction_chars[direction_id]
        distribution_str = ""
        for de in distributions[distribution_id]:
            distribution_str += str(de)
        s = alphabet[x] + str(y + 1) + dir_str + distribution_str
    return s


def print_state_string(state):
    """
    The print_state_string function prints out the current state of the game in a human-readable format.
    The function takes as input a state, and prints out all of the information about that state.
    
    :param state: State to be printed.
    :return:
    """
    stone_str_map = {0: ' ', 1: 'F', 2: 'S', 3: 'C', -1: 'f', -2: 's', -3: 'c'}
    timestep, player, board, stone_reserve = state
    top_board = get_top_board(board)

    for y in range(board.shape[1] - 1, -1, -1):
        row_str = "{}|".format(y + 1)
        for x in range(board.shape[0]):
            row_str += stone_str_map[int(top_board[x, y])]
        print(row_str)
    print("  " + "-"*board.shape[0])
    print("  " + "".join([alphabet[i] for i in range(board.shape[0])]))

    for x in range(board.shape[0]):
        for y in range(board.shape[0]):
            h = 0
            while board[x, y, h] != 0:
                h += 1
            if h > 1: 
                stack_str = "stack({}) at ({}{}): ".format(h, alphabet[x], y + 1)
                for p in board[x, y, :h]:
                    stack_str += stone_str_map[int(p)]
                print(stack_str)


def action_from_id(action_idx, legal_action_idxs):
    """
    The action_from_id function takes a single integer as input and returns the corresponding action.

    :param action_idx: Index of legal action to be taken
    :param legal_action_idxs: Tuple of action encoding index arrays 
    :return: A list of the form [x, y, stone_type] if it is a placement action and [x, y, direction_id, distribution_id] if it is a movement action.
    """
    placement, movement = None, None
    x = legal_action_idxs[0][action_idx]
    y = legal_action_idxs[1][action_idx]
    if legal_action_idxs[2][action_idx] < len(stone_types): # placement
        stone_type = legal_action_idxs[2][action_idx] + 1
        placement = [x, y, stone_type]
    else: # movement
        direction_id = (legal_action_idxs[2][action_idx] - len(stone_types))//len(distributions)
        distribution_id = (legal_action_idxs[2][action_idx] - len(stone_types))%len(distributions)
        movement = [x, y, direction_id, distribution_id]
    action = placement, movement
    return action


def legal_actions(state):
    """
    The legal_actions function returns a mask of legal actions for the current state.
    The mask is an array of shape (board_size, board_size, num_actions). The legal action at each position is set to True.
    
    :param state: Used to check which action are legal.
    :return: A mask of all legal actions at the given state.
    """
    timestep, player, board, stone_reserves = state
    action_mask = np.zeros((board.shape[0], board.shape[1], len(stone_types) + len(direction_vectors) * len(distributions)), dtype=bool)
    legal_actions_list = []

    for x in range(board.shape[0]):
        for y in range(board.shape[1]):
            # placement
            for stone_type in stone_types:
                # only allow placement of flat stones on the first move
                if timestep in [0, 1] and (stone_type == STANDING or stone_type == CAP):
                    continue
                try:
                    placement = [x, y, stone_type]
                    check_placement(state, placement)
                    # set action to legal in actions mask
                    action_mask[x, y, stone_type - 1] = True
                    legal_actions_list.append([placement, None])
                except ValueError as e:
                    pass
            if timestep not in [0, 1]:
                # movement
                for direction_id in range(len(direction_vectors)):
                    for distribution_id in range(len(distributions)):
                        try:
                            movement = [x, y, direction_id, distribution_id]
                            check_movement(state, movement)
                            # set action to legal in actions mask
                            action_mask[x, y, len(stone_types) + direction_id*len(distributions) + distribution_id] = True
                            legal_actions_list.append((None, movement))
                        except ValueError as e:
                            pass
    return legal_actions_list, action_mask


def check_placement(state, placement):
    """
    The check_placement function checks if a placement is valid.
    
    :param state: Current state
    :param placement: Placement action to be evaluated
    :return: True if the placement is valid and false otherwise.
    """
    timestep, player, board, stone_reserves = state
    x, y, stone_type = placement
    # check if stone is available
    stone_idx = 0
    if stone_type == CAP:
        stone_idx = 1
    player_idx = 0
    if player == BLACK:
        player_idx = 1
    if stone_reserves[stone_idx + player_idx*2] < 1:
        raise ValueError("Stone reserve for this stone type is empty.")

    # check feasibility
    if board[x, y, 0] != 0:
        raise ValueError("Can not place stone on an occupied tile.")


def check_movement(state, movement):
    """
    The check_movement function checks whether a given movement is legal.
    It takes as input the state of the game, and a proposed movement.
    The output is True iff the given movement is legal.
    
    :param state: The current state of the game.
    :param movement: The movement to be evaluated.
    :return: A boolean value indicating whether the given movement is valid or not.
    """
    timestep, player, board, stone_reserves = state
    board_size = board.shape[0]
    x, y, direction_id, distribution_id = movement

    # movement specification
    direction = direction_vectors[direction_id]
    distribution = distributions[distribution_id]
    num_moved_stones = np.sum(distribution)
    num_stones_at_pos = (board[x, y, :] != 0).sum()
    num_moves = (distribution != 0).sum()

    # check correctness of stones to be moved
    to_move = board[x, y, max(num_stones_at_pos - num_moved_stones, 0):max(num_stones_at_pos, num_moved_stones)].copy()
    if 0 in to_move:
        raise ValueError("The given movement is infeasible because specified stones are missing.")
    if np.sign(to_move[-1]) != player:
        raise ValueError("The given movement is infeasible because the tile is not controlled by the active player.")

    # check feasibility of movement
    for m in range(1, num_moves + 1):
        num_stones_to_place = distribution[m - 1]
        # determine current stack height at this position
        num_stones_present = 0
        next_x = x + m*direction[0]
        next_y = y + m*direction[1]
        # check whether blocked by end of board
        if (next_x < 0 or next_x > board_size - 1) or (next_y < 0 or next_y > board_size - 1):
            raise ValueError("The given movement is infeasible because it runs into the end of the board.")
        
        # determine stack height at this position
        while board[next_x, next_y, num_stones_present] != 0:
            num_stones_present += 1

        # check whether stack plus moved stones would exceed maximum height
        if num_stones_present + num_stones_to_place > board.shape[2]:
            raise ValueError("The given movement exceeds the maximum stack height.")
        
        # check if blocked by cap or standing stone
        top_stone = np.abs(board[next_x, next_y, num_stones_present - 1])
        if top_stone == CAP:
            raise ValueError("The given movement is infeasible because it runs into a cap stone.")
        if top_stone == STANDING:
            # check if standing stone would be flattened by cap stone
            if not(num_stones_to_place == 1 and np.sum(distribution[m - 1:]) == 1 and np.abs(to_move[-1]) == CAP):
                raise ValueError("The given movement is infeasible because it runs into a standing stone.")


def place_stone(state, placement):
    """
    The place_stone function places a stone on the board.
    
    :param state: The current state of the game.
    :param placement: The placement to be executed.
    :return: The updated board and stone_reserves.
    """
    timestep, player, board, stone_reserves = state
    x, y, stone_type = placement

    # on first move, place stone of the other player
    if timestep in [0, 1]:
        player *= -1

    # check feasibility
    if board[x, y, 0] != 0:
        raise ValueError("Can not place stone on an occupied tile.")

    stone_idx = 0
    if stone_type == CAP:
        stone_idx = 1
    player_idx = 0
    if player == BLACK:
        player_idx = 1
    if stone_reserves[stone_idx + player_idx*2] < 1:
        raise ValueError("Stone reserve for this stone type is empty.")

    # place stone
    board[x, y, 0] = player*stone_type
    # update stone reserves
    stone_reserves[stone_idx + player_idx*2] -= 1

    return board, stone_reserves


def move_stack(board, movement):
    """
    The move_stack function takes a board state and a movement specification as input.
    It then moves the stones at position (x, y) in the given direction according to 
    the specified distribution of stones to move. The function returns an updated board state.
    
    :param board: Current state of the board.
    :param movement :The movement of a stack of stones.
    :return: The board after the move has been executed.
    """
    board_size = board.shape[0]
    x, y, direction_id, distribution_id = movement

    # movement specification
    direction = direction_vectors[direction_id]
    distribution = np.array(distributions[distribution_id])
    num_moved_stones = np.sum(distribution)
    num_stones_at_pos = (board[x, y, :]!= 0).sum()
    num_moves = (distribution != 0).sum()

    # copy stack of stones to be moved
    to_move = board[x, y, max(num_stones_at_pos - num_moved_stones, 0):max(num_stones_at_pos, num_moved_stones)].copy()

    # check feasibility of movement
    movement_coords = []
    movement_stack_heights =[]
    for m in range(1, num_moves + 1):
        num_stones_to_place = distribution[m - 1]
        # determine current stack height at this position
        num_stones_present = 0
        next_x = x + m*direction[0]
        next_y = y + m*direction[1]
        # check whether blocked by end of board
        if (next_x < 0 or next_x > board_size - 1) or (next_y < 0 or next_y > board_size - 1):
            raise ValueError("The given movement is infeasible because it runs into the end of the board.")
        
        # determine stack height at this position
        while board[next_x, next_y, num_stones_present] != 0:
            num_stones_present += 1

        # check whether stack plus moved stones would exceed maximum height
        if num_stones_present + num_stones_to_place > board.shape[2]:
            raise ValueError("The given movement exceeds the maximum stack height.")
        
        # check if blocked by cap or standing stone
        top_stone = np.abs(board[next_x, next_y, num_stones_present - 1])
        if top_stone == CAP:
            raise ValueError("The given movement is infeasible because it runs into a cap stone.")
        if top_stone == STANDING:
            # check if standing stone would be flattened by cap stone
            if not(num_stones_to_place == 1 and np.sum(distribution[m - 1:]) == 1 and np.abs(to_move[-1]) == CAP):
                raise ValueError("The given movement is infeasible because it runs into a standing stone.")

        movement_coords.append((next_x, next_y))
        movement_stack_heights.append(num_stones_present)

    # remove stack of stones that is moved
    board[x, y, max(num_stones_at_pos - num_moved_stones, 0):max(num_stones_at_pos, num_moved_stones)] = 0
    
    # execute movement
    for mi, (move_x, move_y) in enumerate(movement_coords):
        num_stones_to_place = distribution[mi]
        stack_height = movement_stack_heights[mi]
        # flatten standing stones
        if np.abs(board[move_x, move_y, stack_height - 1]) == STANDING:
            assert mi == len(movement_coords) - 1
            top_player = 2*int(board[move_x, move_y, stack_height - 1] > 0) - 1
            board[move_x, move_y, stack_height - 1] = top_player*FLAT
        # move stones according to distribution 
        board[move_x, move_y, stack_height:stack_height + num_stones_to_place] = to_move[:num_stones_to_place]
        to_move = to_move[num_stones_to_place:]
    return board


def count_territory(board):
    """
    The count_territory function counts the number of owned white and black tiles on the board.
    It returns a tuple containing these two values.
    
    :param board: The board to be evaluated.
    :return: The number of white and black tiles.
    """
    white_count = 0
    black_count = 0
    for x in range(board.shape[0]):
        for y in range(board.shape[1]):
            num_stones_present = 0
            while board[x, y, num_stones_present] != 0:
                num_stones_present += 1
            top_stone = board[x, y, num_stones_present - 1]
            if np.abs(top_stone) == FLAT: # only flat stones count
                if top_stone > 0:
                    white_count += 1
                elif top_stone < 0:
                    black_count += 1
    return white_count, black_count


def color_depth_first_search(v, adjacency, color_board):
    """
    The color_depth_first_search function takes in a starting vertex, the adjacency list for the graph, and a color board.
    The function returns an updated color board with all of the vertices that are connected (von Neumann neighborhood) to each other having matching colors.
    
    :param v: The current vertex that is being processed.
    :param adjacency: The adjacency list of each vertex.
    :param color_board: The color of each vertex.
    :return: A list of all the nodes in a connected component starting from v.
    """
    discovered = []
    stack = []
    stack.append(v)
    while len(stack) > 0:
        v = stack.pop()
        color_v = color_board[v[0], v[1]]
        if color_v == 0:
            continue
        if v not in discovered:
            discovered.append(v)
            for v_adj in adjacency[v]:
                color_v_adj = color_board[v_adj[0], v_adj[1]]
                if color_v_adj != 0 and color_v_adj == color_v:
                    stack.append(v_adj)
    return discovered


def check_road_component(component, board_size):
    """
    The check_road_component function checks if a component contains roads. 
    That is it checks if the component connects two opposed edges of the board and returns True or False accordingly.
    
    :param component: Component to be checked.
    :param board_size: Size of the board.
    :return: True or False depending on whether the component contains a road.
    """
    north, east, south, west = False, False, False, False
    for (x, y) in component:
        if x == 0:
            west = True
        elif x == board_size - 1:
            east = True
        if y == 0:
            south = True
        elif y == board_size - 1:
            north = True
    return (north and south) or (west and east)


def check_tak(state, adjacency):
    timestep, player, board, stone_reserves = state

    white_tak = False
    black_tak = False
    
    # simulate all possible actions and check whether they lead to road wins
    legal_actions_list, _ = legal_actions(state)
    #legal_action_idxs = np.nonzero(action_mask)
    for action in range(legal_actions_list):
        #action = action_from_id(action_id, legal_action_idxs)
        # execute actions on test states
        test_board = board.copy()
        test_stone_reserves = stone_reserves.copy()
        test_state = [timestep, player, test_board, test_stone_reserves]
        test_after_state = take_action(test_state, action)
        board_test_after_state = test_after_state[2]
        white_road, black_road = check_road_win(board_test_after_state, adjacency)
        if white_road:
            white_tak = True
        if black_road:
            black_tak = True
    return white_tak, black_tak


def get_num_stones(board, player):
    """
    The get_num_stones function counts the number of stones (normal and cap) on a board for a given player.
    
    :param board: The board.
    :param player: The player.
    :return: integer pair of number of normal and cap stones a player has on the board.
    """
    num_normal_stones = 0
    num_cap_stones = 0
    for x in range(board.shape[0]):
        for y in range(board.shape[1]):
            for h in range(board.shape[2]):
                if board[x, y, h] != 0 and np.sign(board[x, y, h]) == player:
                    if np.abs(board[x, y, h]) == CAP:
                        num_cap_stones += 1
                    else:
                        num_normal_stones += 1
    return num_normal_stones, num_cap_stones


def get_top_board(board):
    """
    The get_top_board function takes a board as input and returns the top stone at each tile.
    
    :param board: The board position.
    :return: 2D array containing the top stone at each tile.
    """
    top_board = np.zeros(tuple(board.shape[:2]))

    for x in range(board.shape[0]):
        for y in range(board.shape[1]):
            # search for top stone
            num_stones_present = 0
            while board[x, y, num_stones_present] != 0:
                num_stones_present += 1
            top_stone = board[x, y, num_stones_present - 1]
            top_board[x, y] = top_stone

    return top_board


def get_color_components(board, adjacency):
    # get stones placed at the top of each tile
    top_board = get_top_board(board)
    flat_cap_mask = np.logical_or(np.abs(top_board) == FLAT, np.abs(top_board) == CAP)
    color_board = np.sign(top_board)*flat_cap_mask

    # search for components
    white_components = []
    black_components = []
    for x in range(board.shape[0]):
        for y in range(board.shape[1]):
            if top_board[x, y] != 0: # only traverse through occupied tiles
                color_xy = np.sign(top_board[x, y])
                # check whether this tile is part of an existing component
                discovered = False
                for component in (white_components if color_xy == WHITE else black_components):
                    if (x, y) in component:
                        discovered = True
                        break
                if not discovered:
                    # find component of this tile
                    component = color_depth_first_search((x, y), adjacency, color_board)
                    (white_components if color_xy == WHITE else black_components).append(component)  
    return white_components, black_components


def check_road_win(board, adjacency):
    """
    The check_road_win function checks whether a player has won the game by creating a road.
    It does this by checking if there are road components for the players.
    
    :param board: Board to be checked.
    :param adjacency: Adjacency matrix of the board.
    :return: Boolean pair indicating which players if any have a road on the board.
    """
    # get stones placed at the top of each tile
    top_board = get_top_board(board)
    flat_cap_mask = np.logical_or(np.abs(top_board) == FLAT, np.abs(top_board) == CAP)
    color_board = np.sign(top_board)*flat_cap_mask

    # search for road connections
    white_roads = False
    black_roads = False
    white_components = []
    black_components = []
    for x in range(board.shape[0]):
        for y in range(board.shape[1]):
            if top_board[x, y] != 0: # only traverse through occupied tiles
                color_xy = np.sign(top_board[x, y])
                # check whether this tile is part of an existing component
                discovered = False
                for component in (white_components if color_xy == WHITE else black_components):
                    if (x, y) in component:
                        discovered = True
                        break
                if not discovered:
                    # find component of this tile
                    component = color_depth_first_search((x, y), adjacency, color_board)
                    (white_components if color_xy == WHITE else black_components).append(component)     
                    # check whether this component contains a road
                    has_road = check_road_component(component, board.shape[0])
                    if has_road and color_xy == WHITE: white_roads = True
                    elif has_road and color_xy == BLACK: black_roads = True

    return white_roads, black_roads

def take_action(state, action):
    # unpack state and action
    timestep, player, board, stone_reserves = state
    placement, movement = action

    # check that first move is placement
    first_move = timestep in [0, 1]
    assert (first_move and movement is None and placement is not None) or not first_move
    
    # execute move
    if placement is not None and movement is None: # placement
        new_board, new_stone_reserves = place_stone(state, placement)
    elif placement is None and movement is not None: # movement
        new_board = move_stack(board, movement)
        new_stone_reserves = stone_reserves
    after_state = [timestep + 1, player, new_board, new_stone_reserves]
    return after_state


class TakGame:
    def __init__(self, board_size=5, board_height=15, num_stones=25, num_capstones=1, komi=0):
        """
        Constructs a game of Tak.

        :param board_size=5: The size of the board.
        :param board_height=10: The maximum stack height.
        :param num_stones=25: The number of normal stones in the game.
        :param num_capstones=1: The number of capstones in the game.
        :param komi=0: Semi-positive integer indicating the final territory advantage of the second player
        """
        assert board_size > 2 and num_stones > 0 and num_capstones > 0 and komi >= 0 and board_height > num_stones//2
        # game settings
        self.board_size = board_size
        self.board_height = board_height
        self.num_stones = num_stones
        self.num_capstones = num_capstones
        self.komi = komi

        # build adjacency map of the board
        self.adjacency = {}
        for x in range(self.board_size):
            for y in range(self.board_size):
                self.adjacency[(x, y)] = set([(min(max(x + d[0], 0), self.board_size - 1),
                                        min(max(y + d[1], 0), self.board_size - 1))
                                        for d in direction_vectors]) - set([(x, y)])
        
        # generate possible movement distributions
        self.move_distributions = {}
        for stones_taken in range(1, self.board_size):
            #set_of_stone_amounts = set(range(0, self.board_size))
            for stone_count in range(stones_taken):
                distr = [0 for _ in range(self.board_size - 1)]
                stones_left = stones_taken
                for position in range(self.board_size - 1):
                    if stones_left >= stone_count:
                        distr[position] += stone_count
                        stones_left -= stone_count
                    else:
                        distr[position] += stones_left
                        stones_left = 0
            #print(distr)


    def reset(self):
        """
        The reset function is called at the beginning of the game and returns the initial state.
        
        :return: Initial state as list of the timestep (0), starting player (WHITE), board array and stone_reserves array
        """
        # define initial states
        board = np.zeros((self.board_size, self.board_size, self.board_height), dtype=np.byte) # clear board
        stone_reserve = np.array([self.num_stones, self.num_capstones]*2).astype(np.byte) # refill stones
        player = WHITE # white begins
        init_state = [0, player, board, stone_reserve]
        return init_state

    def step(self, state, action):
        """
        The step function takes in a state and an action, and returns the next state.
        The step function also returns a reward for taking that action and whether the game has ended.
        The reward is 0 if the game has not ended yet, -1 if the player lost, or 1 if the player won.
        
        :param state: State of the game.
        :param action: Action to be taken.
        :return: The next_state, the reward and whether the game has ended.
        """
        # unpack state and action
        timestep, player, board, stone_reserves = state

        # execute action
        after_state = take_action(state, action)
        
        # check whether game has ended
        terminal, reward = self.is_terminal(after_state)

        # construct next state
        if terminal:
            next_state = after_state
        else:
            next_state = [timestep + 1, -1*player, board, stone_reserves]
        return next_state, reward, terminal

    def is_terminal(self, after_state):
        """
        The is_terminal function checks whether the game is over and returns a boolean and the reward for the current player
        The reward is +1 for winning, -1 for losing and 0 for a draw.
        
        :param after_state: State after a move has been made.
        :return: A boolean value indicating whether the game is over, and a reward.
        """
        timestep, player, board, stone_reserves = after_state
        # check for full board or no stones left
        terminal = False
        if (board[:, :, 0] != 0).sum() >= self.board_size**2: # board filled
            #print("board full")
            terminal = True
        elif stone_reserves[:2].sum() <= 0 or stone_reserves[2:].sum() <= 0: # stones ran out
            #print("reserves empty: ", stone_reserves)
            terminal = True

        #print("terminal flat? ", terminal)

        if terminal: 
            white_count, black_count = count_territory(board)
            # add komi
            black_count += self.komi
            if white_count == black_count:
                reward = 0.5 if player == WHITE else -0.5
            elif white_count > black_count:
                reward = 1 if player == WHITE else -1
            else:
                reward = 1 if player == BLACK else -1
            return True, reward
        
        # check for road win
        white_roads, black_roads = check_road_win(board, self.adjacency)
        if white_roads and not black_roads:
            reward = 1 if player == WHITE else -1
            terminal = True
        elif black_roads and not white_roads:
            reward = 1 if player == BLACK else -1
            terminal = True
        # Dragon clause (active player wins when one move results in both having a road)
        elif white_roads and black_roads:
            reward = 1
            terminal = True
        else: # no road
            reward = 0
            terminal = False

        #print("terminal road?", terminal)
        return terminal, reward
