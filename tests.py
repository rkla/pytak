import unittest
import numpy as np
import tak

class TestTakGame(unittest.TestCase):
    def test_road_win(self):
        game = tak.TakGame()
        boards = []

        # straight roads
        for y in range(game.board_size):
            board_flat = np.zeros((game.board_size, game.board_size, game.board_height))
            board_cap = np.zeros((game.board_size, game.board_size, game.board_height))
            board_flat[:, y, 0] = tak.FLAT
            board_cap[:, y, 0] = tak.CAP
            boards.append(board_flat)
            boards.append(board_cap)
        for x in range(game.board_size):
            board_flat = np.zeros((game.board_size, game.board_size, game.board_height))
            board_cap = np.zeros((game.board_size, game.board_size, game.board_height))
            board_flat[x, :, 0] = tak.FLAT
            board_cap[x, :, 0] = tak.CAP
            boards.append(board_flat)
            boards.append(board_cap)
        
        for board in boards:
            white_roads, black_roads = tak.check_road_win(board, game.adjacency)
            self.assertTrue(white_roads)
            self.assertFalse(black_roads)

    def test_territory_win(self):
        game = tak.TakGame()
        state = game.reset()
        timestep, player, board, stone_reserve = state
        board[:, :, 0] = player*tak.FLAT
        board[:, 0, 0] = -player*tak.FLAT
        board[-1, -1, 0] = player*tak.STANDING
        stone_reserve[0] -= game.board_size
        road_state = [player, board, stone_reserve]
        white_count, black_count = tak.count_territory(board)
        print(white_count, black_count)
        self.assertTrue(white_count == game.board_size**2 - game.board_size - 1)
        self.assertTrue(black_count == game.board_size)

    def test_movement(self):
        game = tak.TakGame()
        state = game.reset()
        timestep, player, board, stone_reserve = state

        # check regular rollout move
        board[0, 0, :3] = np.array([-tak.FLAT, tak.FLAT, tak.FLAT])
        movement = [0, 0, tak.NORTH, 4]
        tak.move_stack(board, movement)
        self.assertTrue(board[0, 1, 0] == -tak.FLAT)
        self.assertTrue((board[0, 2, :2] == np.array([tak.FLAT, tak.FLAT])).all())

        # check flattening standing stone
        board[1:3, 0, 0] = np.array([-tak.CAP, tak.STANDING])
        movement = [1, 0, tak.EAST, 0]
        tak.move_stack(board, movement)
        self.assertTrue((board[2, 0, :2] == np.array([tak.FLAT, -tak.CAP])).all())

    def test_placement(self):
        game = tak.TakGame()
        state = game.reset()
        timestep, player, board, stone_reserve = state
        # place a cap stone
        placement = [1, 1, tak.CAP]
        board, stone_reserves = tak.place_stone(state, placement)
        state[2:] = [board, stone_reserves]
        self.assertTrue(board[1, 1, 0] == -tak.CAP)
        self.assertTrue(stone_reserve[3] == game.num_capstones - 1)

        # check that one cannot place on top of a cap stone
        placement = [1, 1, tak.FLAT]
        self.assertRaises(ValueError, tak.place_stone, state, placement)

    def test_parse_action_string(self):
        self.assertTrue(tak.parse_action_string("a4>1000") == (None, (0, 3, 1, 0)))
        self.assertTrue(tak.parse_action_string("Fe2") == ((4, 1, 1), None))
        self.assertRaises(ValueError, tak.parse_action_string, "action_str")

if __name__ == "__main__":
    unittest.main()