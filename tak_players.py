import cProfile
import tak
import numpy as np


def simple_evaluation_function(state, num_legal_actions, game):
    timestep, player, board, stone_reserves = state
    # build linear evaluation function
    # features
    # - amount of territory x
    # - how many moves are possible x
    # - whether cap stone is available x
    # - number of own stones in self-controlled stacks
    # - size of largest connected component
    # - whether the game has ended

    # territory
    white_count, black_count = tak.count_territory(board)
    feature_territory = player*(white_count - black_count)
    #max_territory = board.shape[0]*board.shape[1]
    #if player == tak.WHITE:
    #    relative_territory = white_count/max_territory
    #elif player == tak.BLACK:
    #    relative_territory = black_count/max_territory
    
    # mobility
    feature_mobility = num_legal_actions/(game.board_size**2 * (3 + 4*len(tak.distributions)))

    # cap stone available
    player_idx = 0
    if player == tak.BLACK:
        player_idx = 1
    feature_capstone = stone_reserves[player_idx*2 + 1] > 0

    # number of own stones in controlled stacks
    num_controlled = 0
    num_in_stacks = 1
    for x in range(board.shape[0]):
        for y in range(board.shape[1]):
            h = 0
            while board[x, y, h] != 0:
                h += 1
            if h > 2:
                num_in_stacks += h - 1
                num_controlled_here = 0
                for stone in board[x, y, :h - 1]:
                    if np.sign(stone) == player:
                        num_controlled_here += 1
                num_controlled += num_controlled_here
    feature_stack_control = num_controlled/num_in_stacks - 0.5

    # largest components
    white_components, black_components = tak.get_color_components(board, game.adjacency)
    max_size_white, max_size_black = 0, 0
    if len(white_components) > 0:
        max_size_white = max([len(comp) for comp in white_components])
    if len(black_components) > 0:
        max_size_black = max([len(comp) for comp in black_components])
    feature_component_threat = player*(max_size_white - max_size_black)

    # check if game is over
    terminal, reward = game.is_terminal(state)
    feature_terminal = reward

    features = np.array([feature_territory, feature_mobility, feature_capstone, feature_stack_control, feature_component_threat, feature_terminal])

    # evaluation weights
    weight_territory = 1
    weight_mobility = 1
    weight_capstone = 0.1
    weight_stack_control = 0.3
    weight_component_size = 0.3
    weight_terminal = 100
    weights = np.array([weight_territory, weight_mobility, weight_capstone, weight_stack_control, weight_component_size, weight_terminal])

    return weights.dot(features)


class RandomPlayer:
    def __init__(self):
        pass

    def act(self, state):
        legal_actions_list, _ = tak.legal_actions(state)
        action_idx = np.random.randint(0, len(legal_actions_list))  
        action = legal_actions_list[action_idx]
        return action
        

class HumanPlayer:
    def __init__(self):
        pass

    def act(self, state):
        legal_actions, aciton_mask = tak.legal_actions(state)
        action_str = input("Please input action: ")
        action = tak.parse_action_string(action_str)
        return action


class AlphaBetaPlayer:
    def __init__(self, game, depth=1):
        self.game = game
        self.search_depth = depth

    def act(self, state):
        self.num_evals = 0
        timestep, player, board, stone_reserves = state
        eval_state = [timestep, player, board.copy(), stone_reserves.copy()]
        self.saved_action = None
        self.evaluation = self.mini_max(eval_state, self.search_depth, -np.inf, +np.inf)
        print("number of evals: ", self.num_evals)
        print(f"Eval(Player {1 if state[1] == 1 else 2}) = {self.evaluation:.4f}")

        if self.saved_action is None:
            raise ValueError("Can not act since game has already ended.")
        else:
            return self.saved_action

    def mini_max(self, state, depth, alpha, beta):
        legal_actions_list, _ = tak.legal_actions(state)
        num_legal_actions = len(legal_actions_list)
        if depth == 0 or num_legal_actions == 0:
            self.num_evals += 1
            return simple_evaluation_function(state, num_legal_actions, self.game)
        max_value = alpha
        for action_idx in range(num_legal_actions - 1, -1, -1):
            action = legal_actions_list[action_idx]
            eval_state = [state[0], state[1], state[2].copy(), state[3].copy()]
            next_state, reward, terminal = self.game.step(eval_state, action)
            value = -self.mini_max(next_state, depth - 1, -beta, -max_value)
            # more efficient: reverse actions here instead of copying state
            if value > max_value:
                max_value = value
                if depth == self.search_depth:
                    self.saved_action = action
                if max_value >= beta:
                    break
        return max_value

class SimpleMCPlayer:
    def __init__(self, game, num_simulations=10):
        self.game = game
        self.num_simulations = num_simulations


    def act(self, state):
        self.action_ids = []
        self.q_values = []
        timestep, player, board, stone_reserves = state

        legal_actions_list, _ = tak.legal_actions(state)
        num_legal_actions = len(legal_actions_list)

        # simulate games starting in given state for each action
        for action_idx in range(num_legal_actions):
            action = legal_actions_list[action_idx]
            rewards = []
            for n in range(self.num_simulations):
                print(f"Sim {n}, action {action_idx}")
                # take action to be evaluated
                eval_state = [timestep, player, board.copy(), stone_reserves.copy()]
                eval_state, reward, terminal = self.game.step(eval_state, action)
                terminal = False
                reward = 0
                while not terminal:
                    # select random action
                    local_legal_actions_list, _ = tak.legal_actions(eval_state)
                    local_action_idx = np.random.randint(0, len(local_legal_actions_list))
                    local_action = local_legal_actions_list[local_action_idx]
                    # take random action
                    eval_state, reward, terminal = self.game.step(eval_state, local_action)
                # add simulation result
                if eval_state[1] == player:
                    rewards.append(reward)
                else:
                    rewards.append(-reward)
            # save mean value estimate
            self.action_ids.append(action_idx)
            print(f"Value action {action_idx} = {np.mean(rewards)}")
            self.q_values.append(np.mean(rewards))

        # select best action according to value estimates
        best_action_idx = np.argmax(self.q_values)
        best_action = tak.action_from_id(best_action_idx, legal_action_idxs)         
        return best_action

class MCTSPlayer:
    def __init__(self):
        pass

    def act(self, state):
        pass


def test_game():
    # set up game and players
    game = tak.TakGame()
    state = game.reset()
    terminal = False

    #white_player = RandomPlayer()
    #black_player = RandomPlayer()
    white_player = AlphaBetaPlayer(game, depth=1)
    black_player = AlphaBetaPlayer(game, depth=1)
    #black_player = SimpleMCPlayer(game, num_simulations=10)

    # run game loop
    reward = 0
    while not terminal:
        # player selects action
        if state[0] % 2 == 0:
            print(f"Move {state[0] // 2}")
            action = white_player.act(state)
        else:
            action = black_player.act(state)
        print(f"Player {1 if state[1] == 1 else 2} takes action {tak.action_string(action)}")
        
        # game performs action
        state, reward, terminal = game.step(state, action)

        # print state
        tak.print_state_string(state)

    # print winning player
    if abs(reward) != 0.5:
        if reward == 1:
            print(f"Player {1 if state[1] == 1 else 2} won.")
        elif reward == -1:
            print(f"Player {2 if state[1] == 0 else 1} won.")
    else:
        print("The game is a draw.")
 
if __name__ == "__main__":
     cProfile.runctx("test_game()", None, locals())